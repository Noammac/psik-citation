const query = `query Get_book_fulldata_by_book_id($book_id: ID!) {
    get_book_fulldata_by_book_id(book_id: $book_id) {
        book_name
        book_authors
        book_year
        book_publishers {
            publisher_name
        }
        book_id
    }
}`;
const punc = ['.', '?', '!'];
const template = <HTMLTemplateElement>document.getElementById('citation')!;

let citations = <HTMLElement>document.getElementById('citations')!;
let search = <HTMLTextAreaElement>document.getElementById('search')!;

async function book_details(book_id: number): Promise<any> {
  return fetch('https://api-ebooks.psik.io/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query: query,
      variables: { 'book_id': book_id }
    })
  }).then(r => r.json())
  .then(obj => obj.data.get_book_fulldata_by_book_id)
  .catch(console.error);
}


function format_field(field: string): string {
  return field + (punc.includes(field.slice(-1)) ? '' : '.');
}


function format_list(list: string[]): string {
  return format_field(list.slice(0, -1).join(', ') + (list.length == 1 ? '' : ' & ') + list[list.length - 1]);
}


function construct_citation(authors: string[], year: number, title: string, publishers: string[]): DocumentFragment {
  let author_string = format_list(authors) + ` (${year}).`;
  let clone = <DocumentFragment>template.content.cloneNode(true);
  clone.querySelector('.authors')!.textContent = author_string;
  clone.querySelector('.title')!.textContent = format_field(title);
  clone.querySelector('.publisher')!.textContent = format_list(publishers);
  return clone;
}


async function book_citation(book_id: number): Promise<DocumentFragment> {
  return book_details(book_id).then(data => construct_citation(data.book_authors, data.book_year, data.book_name, data.book_publishers.map((pub: {publisher_name: string}) => pub.publisher_name)))
}


search.addEventListener('keyup', function(event) {
  console.log('Event');
  if (event.key === "Enter") {
    search.value.split(',').map(id => book_citation(parseInt(id))).forEach(prm => prm.then(df => citations.appendChild(df)))
  }
  event.preventDefault();
});
